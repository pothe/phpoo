<?php
//Script en POO con condicionales y arreglos, guardando e imprimiendo una lista de enlaces

//Declaración de clase
class Menu {
	//Declaración de atributos con asignación de arreglo
	private $no=array();
	private $link=array();
	private $titulo=array();

	//Declaración de métodos
	public function cargaEnlace($enlace,$title){
		$this->link[]=$enlace;
		$this->titulo[]=$title;
	}

	//Declaración de método para imprimir
	public function ver()
	{
		//Declaración de for 
		for ($i=0; $i < count($this->link); $i++) { 
			//Imprime los elementos del arreglo contenidos en el atributo
			echo '<a href="'.$this->link[$i].'">'.$this->titulo[$i].'</a><br>';
		}//Fin del for
	}//Fin de la función ver
}//Fin de la clase Menú

//Declaración del objeto
$menu=new Menu();
//Carga de enlaces a imprimir
$menu->cargaEnlace('https://nuve.ga','Nuve.ga');
$menu->cargaEnlace('https://nuve.gq','Nuve.gq');
$menu->cargaEnlace('https://wzcl.gq','wzcl.gq');
//Impresión de los enlaces
$menu->ver();

?>