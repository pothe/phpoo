<?php
//Script para realizar suma con una clase, tres atributos y dos métodos

//Declaración de la clase
class Numeros {
	//Declaración de los atributos
	private $uno;
	private $dos;
	public $tres;

	//Declaración de los métodos
	public function Inicio($one,$two)
	{
		//Asignación de los parámetros a los atributos
		$this->uno=$one;
		$this->dos=$two;
		$this->tres=$one+$two;
	}//Fin de Inicio

	public function Ver()
	{	
		//Impresión de un atributo
		echo $this->tres;
	}//Fin de Ver
}//Fin de la clase

//Declaración de un objeto asignando la palabra 'new' y el nombre de la clase
$suma=new Numeros;
//Asignación de valores a los parametros
$suma->Inicio(6,9);
//Impresión de la suma
$suma->Ver();
echo "<br>";
$num=new Numeros;
$num->Inicio(45,89);
$num->Ver();
?>