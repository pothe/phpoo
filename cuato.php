<?php
class usuario{
	private $name;
	private $edad;
	private $pago;

	public function __construct($nombre,$old,$coin)
	{
		$this->name=$nombre;
		$this->edad=$old;
		$this->pago=$coin;
	}//Fin del constructor

	public function datos()
	{
		echo "Nombre del cliente: ".$this->name;
		echo "<br>Edad: ".$this->edad;
		echo "<br>Cantidad pagada: ".$this->pago;
	}//Fin de método datos

}//Fin de clase usuario

class renta{
	private $cuarto;
	private $bande;

	public function rento($cosa)
	{
		$this->cuarto=$cosa;
	}

	private function edad()
	{
		if ($this->cuarto->edad<18) {
			$this->bande="<br>Es menor de edad";
		}
		else{
			$this->bande="<br>Es adulto";
		}
	}//Fin del método edad

	public function ver()
	{
		$this->cuarto->datos();

		echo $this->bande;
	}//Fin del método ver
}//Fin de la clase renta

$algo=new usuario('Marco Reyes',24,500);
$hotel=new renta;
$hotel->rento($algo);
$hotel->ver();
?>