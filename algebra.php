<?php
//Script que realiza suma y resta de 2 números con POO en PHP

//Declara la clase
class algebra {
	//Declaración de atributos privados y público
	private $uno;

	private $dos;

	public $tres;

	//Delcaración de métodos
	public function suma($one,$two){
		$this->uno=$one;
		$this->dos=$two;
		$this->tres=$one+$two;
	}

	public function resta($one,$two){
		$this->uno=$one;
		$this->dos=$two;
		$this->tres=$one-$two;
	}

	public function ver (){
		echo $this->tres;
	}
}

//Declaración de un objeto
$mil=new algebra;
//Llamado del método con envió de parámetros
$mil->suma(5,8);
//Impresión de la suma
$mil->ver();

echo "<br>\n";
//Declaración de otro objeto
$cien=new algebra;
//Llamar otro método con envio de parametros
$cien->resta(90,76);
//Impresión de la resta
$cien->ver();
echo "<br>";

//Impresión de un atributo público
//echo $mil->tres."<br>".$cien->tres;
?>