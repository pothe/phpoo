<?php
//Script con condicional

//Declaración de clase
class Empleado {
	//Declaración de atributos
	private $nombre;
	private $sueldo;

	//Declaración de métodos
	public function iniciar($name,$salt){
		$this->nombre=$name;
		$this->sueldo=$salt;
	}

	//Declaración de método ver
	public function ver(){
		echo "Nombre de Empleado: ".$this->nombre;
		echo "<br>Sueldo: ".$this->sueldo;
		echo "<br>Paga Impuestos: ";
		if ($this->sueldo>3000) {
			echo "Si";
		}
		else{
			echo "No";
		}//Fin del else
		echo "<br><br>";
	}//Fin de función ver
}//Fin de la clase

//Declarar objeto
$god=new Empleado;
//Envió de parametros a la función
$god->iniciar('Ursula',2500);
//Llamar función para imprimir
$god->ver();

$mar=new Empleado;
$mar->iniciar('Nidia',30001);
$mar->ver();
?>