<?php
//Script de menú dinámico con llamada interna de métodos

//Declaración de clase
class Menu {
	//Declaración de atributo con asignación de arreglo
	private $opcion=array();

	//Declaración de métodos
	//Función que carga opciones
	public function cargaOpciones ($punto)
	{
		//El parametro se asigna en el arreglo
		$this->opcion[]=$punto;
	}//Fin de la función

	//Función para imprimir horizontalmente
	private function horizontal()
	{
		for ($i=0; $i < count($this->opcion); $i++) { 
			echo $this->opcion[$i].' ';
		}//Fin del for
	}//Fin de función ver

	//Función para imprimir verticalmente
	private function vertical()
	{
		for ($i=0; $i < count($this->opcion); $i++) { 
			echo $this->opcion[$i].'<br>';
		}//Fin del for
	}//Fin de vertical

	//Función que llama a las funciones de imprimir según el parametro
	public function ver($opcion)
	{
		//Condicional que llama al método conforme al parametro
		if ($opcion==0) {
			//Si parametro == 0 se imprime horizontal
			$this->horizontal();
		}
		else
		{
			//Si parametro es /=0 se imprime verticalmente el menú
			$this->vertical();
		}//Fin del else
	}//Fin de función ver

}//Fin de la clase-

//Declaración del objeto
$va=new Menu;
//Carga de opciones
$va->cargaOpciones('Nuevo');
$va->cargaOpciones('Abrir');
$va->cargaOpciones('Cargar');
//Llamada de función imprimir y en el parametro como se imprime el menú
$va->ver(0);

?>