<?php
//Este script es para imprimir un nombre con una clase y dos métodos

//Se declara la clase
class Gancho {
	//Declaración de atributos como variables
	public $nombre;

	//Declaración de métodos como funciones
	public function inicio($name){
		$this->nombre=$name;
	}//Fin de la función

	public function imprime()
	{
		echo $this->nombre;
		echo "<br>";
	}//Fin de imprime

}//Fin de Gancho

//Creación de un objeto asignando la palabra 'new' y el nombre de la clase
$pe=new Gancho();

//Llamada de un método enviando en su parámetro
$pe->inicio('Marco');

//Llamada del método imprimir
$pe->imprime();

//Se le cambia el valor al atributo publico
$pe->nombre='Nina';

//Se imprime el atributo modificado
echo $pe->nombre;

//Se vuelve a imprimir el método de imprimir pero el valor ya no es el original
$pe->imprime();

?>