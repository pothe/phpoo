# RePOO de Scripts PHP

## Este RePOOsitorio contiene archivos PHP en Orientado a Objetos


**Los scripts aquí contenidos son para probar el potencial de la Programación Orientada a Objetos (POO) en el lenguaje PHP**

### Archivos que contiene:

Archivo | Descripción
---- | -------
names.php | Imprime el nombre con clase, atributo y métodos
suma.php | Suma 2 números con POO
algebra.php | Suma y resta de 2 números con POO
zelda.php | Impresión de enlaces con arreglos y for
sueldo.php | Condicíonal si se paga o no impuestos
opcion.php | Llamada de métodos internamente con condicional
ping.php | Realiza un ping con un constructor
cuato.php | Envio de un objeto dentro de una clase

#### Scripts desarrollados por: Marco Antonio Castillo Reyes